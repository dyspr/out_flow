var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var particles = []
var particlePush = setInterval(pushParticle, 1)

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  particles.push(new Particle())
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  push()
  translate(windowWidth * 0.5, windowHeight * 0.5)
  for (var i = 0; i < particles.length; i++) {
    particles[i].display()
    particles[i].move()

    if (particles[i].popIt()) {
      particles.splice(i, 1)
    }
  }
  pop()
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function pushParticle() {
  particles.push(new Particle())
}

class Particle {
  constructor() {
    this.dir = 2 * (frameCount % (sin(frameCount * 0.001))) / (sin(frameCount * 0.001))
    this.speed = 0.0025
    this.size = 0.0
    this.dist = 0
    this.grow = 0.005
    this.color = 1
    this.fade = 0.0165
  }

  move() {
    this.dist += this.speed
    this.size += this.grow
    this.color -= this.fade
  }

  display() {
    fill(sin(this.color) * 128 + 128)
    noStroke()
    ellipse(sin(Math.PI * this.dir) * this.dist * boardSize, cos(Math.PI * this.dir) * this.dist * boardSize, sin(this.size) * boardSize * 0.2)
  }

  popIt() {
    if (this.color <= -0.5 * Math.PI) {
      return true
    }
  }
}
